<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SalesOrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login',[AuthController::class,'login']);
Route::post('/register',[AuthController::class,'register']);

Route::middleware('jwt.auth')->group(function(){
    Route::prefix('app')->group(function(){
        Route::resource('/customers',CustomerController::class)->except(['create','show']);
        Route::resource('/products',ProductController::class)->except(['create','show']);;
        Route::get('sales-order',[SalesOrderController::class,'index']);
        Route::post('sales-order',[SalesOrderController::class,'store']);
    });
});
