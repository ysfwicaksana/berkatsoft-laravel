<?php

namespace App\Http\Controllers;

use App\Http\Requests\SalesOrder\SalesOrderRequest;
use App\Http\Services\SalesOrderService;
use Illuminate\Support\Facades\DB;

class SalesOrderController extends Controller
{
    public function index(){

        $salesOrder = DB::table('sales_order')
                          ->select('customer_name','product_name','sales_order.created_at')
                          ->join('products','products.id','=','sales_order.product_id')
                          ->join('customers','customers.id','=','sales_order.customer_id')
                          ->orderBy('sales_order.created_at','desc')
                          ->get();
        
        return response()->json($salesOrder,200);
    }

    public function store(SalesOrderRequest $request, SalesOrderService $salesOrderService){

        $salesOrderService->createSalesOrder($request->validated());
        return response()->json('Order created!',201);

    }
}
