<?php

namespace App\Http\Controllers;

use App\Http\Services\AuthService;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;

class AuthController extends Controller
{
    
    public function login(LoginRequest $request,AuthService $authService){

        $user = User::where('email',$request->email)->first();
        $token = $authService->createToken($request->validated());

        if($token) {

            return response()->json([
                'message' => 'Token created',
                'token' => 'Bearer '.$token,
                'name' => $user->name
            ],200);
        } else {
            return response()->json(
                 'Email or password not found'
            ,422);
        }
        
    }

    public function register(RegisterRequest $request,AuthService $authService) {

        
        $authService->createAccount($request->safe()->except(['password_confirmation']));
        $token = $authService->createToken($request->safe()->only(['email','password']));

        if($token) {

            return  response()->json([
                        'message' => 'Token created',
                        'token' => 'Bearer '.$token,
                    ],200);
        } else {
            return  response()->json([
                        'message' => 'Email or password not found',
                    ],422);
        }             
    }
}
