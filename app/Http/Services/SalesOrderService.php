<?php

namespace App\Http\Services;

use App\Models\SalesOrder;

class SalesOrderService {

    public function createSalesOrder($order) {

        foreach($order['product_list'] as $product) {

            SalesOrder::create([
                'product_id' => $product['product_id'],
                'customer_id' => $order['customer_id']
            ]);
        }
    }
}