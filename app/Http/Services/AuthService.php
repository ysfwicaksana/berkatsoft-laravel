<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT;
use App\Models\User;

class AuthService {

    public function createToken($user)  {

        if(Auth::attempt($user)) {

            return JWT::encode([
                'iss' => Auth::user()->name,
                'iat' => now()->timestamp,
                'exp' => now()->timestamp + 60*60*2,
            ],env('JWT_SECRET_KEY'),'HS256');
            
        }

        return false;
    }

    public function createAccount($user) {
        User::create([
            'name' => $user['name'],
            'email' => $user['email'],
            'password' => Hash::make($user['password'])
        ]);

    }
}