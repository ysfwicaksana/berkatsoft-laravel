<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|max:100|required',
            'email' => 'bail|unique:users,email|required',
            'password' => ['bail','required','confirmed',Password::min(8)->letters()->mixedCase()->numbers()->symbols()],
            'password_confirmation' => 'bail|required'
        ];
    }

     /**
    * [failedValidation [Overriding the event validator for custom error response]]
    * @param  Validator $validator [description]
    * @return [object][object of various validation errors]
    */
    public function failedValidation(Validator $validator) { 

        $errMessages = '<ul>';
        foreach($validator->errors()->all() as $error) {
            $errMessages .= '<li>'.$error.'</li>'; 
        }
        $errMessages .= '</ul>';
        
        throw new HttpResponseException(response()->json($errMessages, 422)); 
    }
}
