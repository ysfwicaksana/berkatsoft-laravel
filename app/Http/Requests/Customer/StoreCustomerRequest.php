<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name' => 'required|max:100',
            'customer_address' => 'required',
            'customer_phone_number' => 'required|max:20'        
        ];
    }

    /**
    * [failedValidation [Overriding the event validator for custom error response]]
    * @param  Validator $validator [description]
    * @return [object][object of various validation errors]
    */
    public function failedValidation(Validator $validator) { 
        $errMessages = '<ul>';
        foreach($validator->errors()->all() as $error) {
            $errMessages .= '<li>'.$error.'</li>'; 
        }
        $errMessages .= '</ul>';
       throw new HttpResponseException(response()->json($errMessages, 422)); 
    }
}
