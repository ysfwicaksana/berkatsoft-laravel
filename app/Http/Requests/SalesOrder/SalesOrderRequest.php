<?php

namespace App\Http\Requests\SalesOrder;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class SalesOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required',
            'product_list' => 'required',
            
        ];
    }

    public function failedValidation(Validator $validator) { 
        $errMessages = '<ul>';
        foreach($validator->errors()->all() as $error) {
            $errMessages .= '<li>'.$error.'</li>'; 
        }
        $errMessages .= '</ul>';
       throw new HttpResponseException(response()->json($errMessages, 422)); 
    }
}
