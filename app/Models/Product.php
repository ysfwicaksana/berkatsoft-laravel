<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['product_name','product_price','expired_at'];

    public function customers(){
        return $this->belongsToMany(Customer::class,'sales_order');
    }
}
