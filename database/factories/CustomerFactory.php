<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Customer;

class CustomerFactory extends Factory
{
    protected $model = Customer::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */

    
    public function definition()
    {
        return [
            'customer_name' => $this->faker->name(),
            'customer_address' => $this->faker->city(),
            'customer_phone_number' => $this->faker->phoneNumber()
        ];
    }
}
