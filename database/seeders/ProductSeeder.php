<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        
       $products = [
            [

                'product_name' => 'Sabun Lifebouy',
                'product_price' => 3000,
                'expired_at' => '2023-09-09'
            ],
            
            [
                'product_name' => 'Sarimie',
                'product_price' => 3000,
                'expired_at' => '2023-09-09'
            ],
            [
                'product_name' => 'Cassablanca Deodorant',
                'product_price' => 20000,
                'expired_at' => '2023-09-09'
            ],
            [
                'product_name' => 'Rinso Cair',
                'product_price' => 15000,
                'expired_at' => '2023-09-09'
            ],
            [
                'product_name' => 'Tepung Sajiku',
                'product_price' => 5000,
                'expired_at' => '2023-09-09'
            ],
            [
                'product_name' => 'Pepsodent',
                'product_price' => 15000,
                'expired_at' => '2023-09-09'
            ],
        ];


        foreach($products as $product) {
            Product::create([
                'product_name' => $product['product_name'],
                'product_price' => $product['product_price'],
                'expired_at' => $product['expired_at'],
            ]);
        }
    }
}
